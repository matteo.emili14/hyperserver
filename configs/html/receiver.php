<?php 
require_once('config.php');
$porta = $_GET['porta'];
$type = $_GET['type'];

global $servername, $username, $password, $dbname;
$conn = new mysqli($servername, $username, $password, $dbname);

$id = substr($porta, -1);

if($type == "aggiungi")
{
    $utente = $_GET['utente'];
    $profilo = $_GET['profilo'];
    $password = $_GET['password'];
    //imposta nuovo profilo
    shell_exec("sudo iptables -A OUTPUT -t mangle -p tcp --sport ".$porta." -j MARK --set-mark ".$profilo.$id);

    $conn->query("INSERT INTO `rubrica` (`porta`, `utente`, `profilo`) VALUES ('".$porta."', '".$utente."', '".$profilo."');");
    $conn->query("INSERT INTO `logDati` (`porta`, `utente`, `datiTotaliMB`) VALUES ('".$porta."', '".$utente."', '0');");

    shell_exec("sudo useradd -s /bin/bash ".$utente);
    shell_exec("sudo echo ".$utente.":".$password." | sudo chpasswd");
}
else if($type == "rimuovi")
{
    //CANCELLAZIONE UTENTE DAL SERVER
    shell_exec("sudo userdel -r ".getUtente($porta));
    //shell_exec("sudo iptables -D INPUT -p tcp -m tcp --dport ".$porta." --tcp-flags FIN,SYN,RST,ACK SYN -m connlimit --connlimit-above 1 --connlimit-mask 32 --connlimit-saddr -j REJECT --reject-with tcp-reset"); //rimuovo regola standard prima di attivare il blocco per disconnessione
    shell_exec("sudo iptables -D INPUT -p tcp --syn --dport ".$porta." -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset");
    //shell_exec("sudo sh /root/closeConnection.sh ".$porta);
    shell_exec("sudo tcpkill -i eth0 -9 port ".$porta."> /dev/null 2>/dev/null &");
    sleep(30);
    $pid = shell_exec("sudo pidof tcpkill");
    shell_exec("sudo kill ".$pid);
    shell_exec("sudo iptables -A INPUT -p tcp --syn --dport ".$porta." -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset"); //ripristino regola standard

    //PROCEDURA RIMOZIONE UTENTE
    if(limitato($porta)) //CONTROLLO SE LIMITATO E RIMUOVO PROFILO
    {
        shell_exec("sudo iptables -D OUTPUT -t mangle -p tcp --sport ".$porta." -j MARK --set-mark 2".$id);
    }
    else //SE NON LIMITATO RIMUOVO PROFILO TRADIZIONALE
    {
        $profilo = getProfilo($porta);
        shell_exec("sudo iptables -D OUTPUT -t mangle -p tcp --sport ".$porta." -j MARK --set-mark ".$profilo.$id);
    }

    $conn->query("DELETE FROM `rubrica` WHERE  `porta`='".$porta."';");
    $conn->query("DELETE FROM `logDati` WHERE  `porta`='".$porta."';");
}
else //upgrade
{
    $profiloVecchio = getProfilo($porta);
    $profilo = $_GET['profilo'];

    if(limitato($porta)) //CONTROLLO SE LIMITATO E RIMUOVO PROFILO
    {
        shell_exec("sudo iptables -D OUTPUT -t mangle -p tcp --sport ".$porta." -j MARK --set-mark 2".$id);
    }
    else //SE NON LIMITATO RIMUOVO PROFILO TRADIZIONALE
    {
        shell_exec("sudo iptables -D OUTPUT -t mangle -p tcp --sport ".$porta." -j MARK --set-mark ".$profiloVecchio.$id);
    }

    shell_exec("sudo iptables -A OUTPUT -t mangle -p tcp --sport ".$porta." -j MARK --set-mark ".$profilo.$id);
    $conn->query("UPDATE `rubrica` SET `profilo`='".$profilo."' WHERE `porta`=".$porta.";");
}

$conn->close();
shell_exec("sudo invoke-rc.d netfilter-persistent save"); //salvo tutte le modifiche alle iptables

function limitato($porta)
{
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $result = $conn->query("SELECT limitato FROM rubrica WHERE porta = '".$porta."';");
    $conn->close();

    $fetched = $result->fetch_assoc()['limitato'];

    if($fetched == 0)
        return false;
    else
        return true;
}

function getProfilo($porta)
{
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $result = $conn->query("SELECT profilo FROM rubrica WHERE porta = '".$porta."';");
    $conn->close();

    return $result->fetch_assoc()['profilo'];
}

function getUtente($porta)
{
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $result = $conn->query("SELECT utente FROM rubrica WHERE porta = '".$porta."';");
    $conn->close();

    return $result->fetch_assoc()['utente'];
}
?>