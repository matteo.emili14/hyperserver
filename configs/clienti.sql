-- --------------------------------------------------------
-- Host:                         hyperserver02.ns01.info
-- Versione server:              5.5.59-0+deb8u1 - (Debian)
-- S.O. server:                  debian-linux-gnu
-- HeidiSQL Versione:            9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dump della struttura del database clienti
CREATE DATABASE IF NOT EXISTS `clienti` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `clienti`;

-- Dump della struttura di tabella clienti.logDati
CREATE TABLE IF NOT EXISTS `logDati` (
  `porta` int(11) NOT NULL DEFAULT '80',
  `utente` varchar(50) NOT NULL DEFAULT 'utente',
  `datiTotaliMB` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`porta`),
  UNIQUE KEY `utente` (`utente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- L’esportazione dei dati non era selezionata.
-- Dump della struttura di tabella clienti.rubrica
CREATE TABLE IF NOT EXISTS `rubrica` (
  `porta` int(11) NOT NULL DEFAULT '80',
  `utente` varchar(50) NOT NULL,
  `profilo` int(11) NOT NULL DEFAULT '50',
  `dati` int(11) NOT NULL DEFAULT '0',
  `datiOrari` int(11) NOT NULL DEFAULT '0',
  `limitato` int(1) NOT NULL DEFAULT '0',
  `dataCreazione` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`porta`),
  UNIQUE KEY `rifCliente` (`utente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- L’esportazione dei dati non era selezionata.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
