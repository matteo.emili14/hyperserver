<?php
require_once('config.php');

checkForOrdersToProcess();

function checkForOrdersToProcess()
{
    global $servername, $username, $password, $dbname, $dbname2, $serverList, $slotPerServer;
    $conn = new mysqli($servername, $username, $password, $dbname);
    
    $orders = getOrders();

    if($orders == null)
        return;
    else
    {
        while($row = $orders->fetch_assoc())
        {
            $id_customer = $row['id_customer'];
            $id_order = $row['id_order'];
            $id_cart = $row['id_cart'];
            $customerData = getCustomerDetails($id_customer);
            $firstName = $customerData['firstname'];
            $lastName = $customerData['lastname'];
            $email = $customerData['email'];
            $purchaseData = getPurchaseInfo($id_cart);
            $duration = $purchaseData['duration'];
            $profile = $purchaseData['profile'];

            $serverData = hasAvailableSlot($serverList, $slotPerServer);
            $server = $serverData['server'];
            $port = $serverData['port'];
            //trovare server e porta disponibile

            $user = generateUser($email);

            manageActivation($firstName, $lastName, $user, $email, $profile, $server, $port, $duration);

            $conn->query("UPDATE ps_orders SET processed=1 WHERE id_order='".$id_order."';");
        }
    }
    $conn->close();
    startProcessResellerActivations(); //avvio controllo e attivazione su ordini reseller, così è sufficiene mettere solo questo file in crontab
}


function startProcessResellerActivations()
{
    require_once('processResellerActivations.php');
    checkForResellerOrdersToProcess();
}
//quando l'ordine è presente nel db prestashop con flag current_state=2 e processed=0 -> attivazione da avviare
//quando l'ordine è presente nel db prestashop con flag current_state=2 e proccesed=0 ma è presente anche nel db hyperserver -> in attivazione
//quando l'ordine è presente nel db prestashop con flag current_state=2 e proccesed=1 -> attivazione completata

function manageActivation($firstName, $lastName, $user, $email, $profile, $server, $port, $duration)
{
    require_once(dirname(__FILE__).'/gmail.php');

    global $servername, $username, $password, $dbname2;
    $conn = new mysqli($servername, $username, $password, $dbname2);
    $result = $conn->query("SELECT * FROM rubrica_globale WHERE email='".$email."';");

    $data = $result->fetch_assoc();

    if($result->num_rows > 0) //controllo se ha/avuto abbonamento
    {
        if(time() < strtotime($data['dataTermine'])) //controllo se è attualmente in essere
        {
            //prolungo abbonamento o eseguo upgrade
            $activeServerPort = $data['porta'];
            $activeServer = $data['server'];
            $activeProfile = $data['profilo'];

            $endDate = updateCustomer($email, $profile, $activeServer, $activeServerPort, $duration, true);

            if(($profile != $activeProfile) && (substr($duration, -1) != "g")) //è anche un upgrade
            {
                configureServer($user, $profile, $activeServer, $activeServerPort, $data['password'], true); //eseguo upgrade
            }

            $ehiPaths = findEhi($server, $port);
            sendMail($email, $user, $firstName, $data['password'], normalizeTimestamp($endDate), $ehiPaths, true, false); //non nuova attivazione ma già attiva
        }
        else //abbonamento scaduto
        {
            //non c'è necessità di creare un nuovo utente, basta configurare un server con i dati già presenti e rimuovere flag scaduto
            $endDate = updateCustomer($email, $profile, $server, $port, $duration, false);
            configureServer($user, $profile, $server, $port, $data['password'], false);

            $ehiPaths = findEhi($server, $port);
            $conn->query("UPDATE `rubrica_globale` SET `scaduto`='0' WHERE `email`='".$email."';");
            sendMail($email, $user, $firstName, $data['password'], normalizeTimestamp($endDate), $ehiPaths, false, false); //non nuova attiva linea disattiva
        }
    }
    else //mai avuto abbonamento
    {
        $sshPassword = generateRandomString();
        $endDate = createCustomer($firstName, $lastName, $email, $sshPassword, $profile, $server, $port, $duration);
        configureServer($user, $profile, $server, $port, $sshPassword, false);
        $ehiPaths = findEhi($server, $port);
        sendMail($email, $user, $firstName, $sshPassword, normalizeTimestamp($endDate), $ehiPaths, false, true); //nuova attivazione
    }

    $conn->close();
}

function findEhi($server, $port)
{
    $baseLocalPath = "/var/www/html/ehi/";
    $baseOnlinePath = "http://hyperspeed.it/ehi/";
    $id = substr($port, -1);
    $serverPath = explode(".", $server);
    $ext = ".ehi";

    $timLocalPath = $baseLocalPath.$serverPath[0]."/".$id."-tim".$ext;
    $vodafoneLocalPath = $baseLocalPath.$serverPath[0]."/".$id."-vodafone".$ext;
    $windLocalPath = $baseLocalPath.$serverPath[0]."/".$id."-wind".$ext;

    $timOnlinePath = $baseOnlinePath.$serverPath[0]."/".$id."-tim".$ext;
    $vodafoneOnlinePath = $baseOnlinePath.$serverPath[0]."/".$id."-vodafone".$ext;
    $windOnlinePath = $baseOnlinePath.$serverPath[0]."/".$id."-wind".$ext;

    return array("timLocalPath"=>$timLocalPath, "vodafoneLocalPath"=>$vodafoneLocalPath, "windLocalPath"=>$windLocalPath, "timOnlinePath"=>$timOnlinePath, "vodafoneOnlinePath"=>$vodafoneOnlinePath, "windOnlinePath"=>$windOnlinePath);
}

function createCustomer($firstName, $lastName, $email, $sshPassword, $profile, $server, $port, $duration)
{
    global $servername, $username, $password, $dbname2;
    $conn = new mysqli($servername, $username, $password, $dbname2);

    if(substr($duration, -1) == "g") //durata in giorni
    {
        $duration = date('Y-m-d', time()+(intval(substr($duration, 0, -1))*24*60*60));
    }
    else if(substr($duration, -1) == "m") //durata in mesi
    {
        $todayTimestamp = date('Y-m-d', time());
        $duration = endCycle($todayTimestamp, substr($duration, 0, -1));
    }
    //0000-00-00 ANNO-MESE-GIORNO
    $conn->query("INSERT INTO rubrica_globale (`nome`, `cognome`, `email`, `password`, `profilo`, `server`, `porta`, `dataInizio`, `dataTermine`) VALUES ('".$firstName."', '".$lastName."', '".$email."', '".$sshPassword."','".$profile."', '".$server."', '".$port."', '".date('Y-m-d H:i:s', time())."', '".$duration." 00:00:00');");
    $conn->close();

    return $duration;
    //da eseguire solo se l'utente NON esiste già
}

function updateCustomer($email, $profile, $server, $port, $duration, $alreadyActive)
{
    global $servername, $username, $password, $dbname2;
    $conn = new mysqli($servername, $username, $password, $dbname2);
    //0000-00-00 ANNO-MESE-GIORNO
    
    if($alreadyActive) //abbonamento attivo da prolungare
    {
        $result = $conn->query("SELECT date(dataTermine) AS dataTermine FROM rubrica_globale WHERE email='".$email."';");
        $timestampEndDate = $result->fetch_assoc()['dataTermine'];
        
        if(substr($duration, -1) == "g") //durata in giorni
        {
            $duration = date('Y-m-d', strtotime($timestampEndDate)+(intval(substr($duration, 0, -1))*24*60*60));
        }
        else if(substr($duration, -1) == "m") //durata in mesi
        {
            $duration = endCycle($timestampEndDate, substr($duration, 0, -1));
        }

        $conn->query("UPDATE rubrica_globale SET profilo='".$profile."', dataTermine='".$duration." 00:00:00' WHERE email='".$email."';");
        $conn->close();

        return $duration;
    }
    else //abbonamento scaduto da rinnovare
    {
        if(substr($duration, -1) == "g") //durata in giorni
        {
            $duration = date('Y-m-d', time()+(intval(substr($duration, 0, -1))*24*60*60));
        }
        else if(substr($duration, -1) == "m") //durata in mesi
        {
            $todayTimestamp = date('Y-m-d', time());
            $duration = endCycle($todayTimestamp, substr($duration, 0, -1));
        }
        $conn->query("UPDATE rubrica_globale SET profilo='".$profile."', server='".$server."', porta='".$port."', dataInizio='".date('Y-m-d H:i:s', time())."', dataTermine='".$duration." 00:00:00' WHERE email='".$email."';");
        $conn->close();

        return $duration;
    }
}

function configureServer($user, $profile, $server, $port, $sshPassword, $alreadyActive = false)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if($alreadyActive)
    {
        //upgrade/downgrade di utenza già attiva
        curl_setopt($ch, CURLOPT_URL, "http://".urlencode($server).":8080/receiver.php?porta=".$port."&profilo=".$profile."&type=upgrade");
    }
    else
    {
        //hyperserver03.ns01.info:8080/receiver.php?porta=83&utente=matem&password=Pexe8eVb&profilo=30&type=aggiungi
        curl_setopt($ch, CURLOPT_URL, "http://".urlencode($server).":8080/receiver.php?porta=".$port."&utente=".urlencode($user)."&password=".urlencode($sshPassword)."&profilo=".$profile."&type=aggiungi");
        //configurazione ex novo
    }

    curl_exec($ch);
    curl_close($ch);
}

function hasAvailableSlot($serverList, $slotPerServer)
{
    $server = "";
    $port = 0;
    $available = false;
    for($i = 0; $i < count($serverList); $i++)
    {
        if($serverList[$i] != null) //controllo per mantenere allineati posizione elementi con nome (01 non è stato assegnato quindi manca tale posizione che è null)
        {
            $jsonObject = parseJson($serverList[$i]);
            if(count($jsonObject) < $slotPerServer) //se gli elementi in rubrica di un server sono minori degli slot massimi -> spazio disponibile
            {
                $server = $serverList[$i];
                $port = getAvailablePortOnServer($jsonObject);
                $available = true;
                break;
            }
        }
    }

    if($available)
        return array("server"=>$server, "port"=>$port);
    else
        return;
}

function getAvailablePortOnServer($jsonWithData)
{
    global $sshPorts;
    $alreadyUsedPorts = array();

    for($i = 0; $i < count($jsonWithData); $i++)
    {
        array_push($alreadyUsedPorts, intval($jsonWithData[$i]['porta']));
    }
    $freePorts = array_diff($sshPorts, $alreadyUsedPorts);

    return reset($freePorts); //prendo la prima porta libera;
}

function getOrders()
{
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $result = $conn->query("SELECT id_order,id_customer,id_cart FROM ps_orders WHERE current_state=2 AND processed=0;");
    $conn->close();

    return $result;
}

function getCustomerDetails($id_customer)
{
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $result = $conn->query("SELECT firstname,lastname,email FROM ps_customer WHERE id_customer='".$id_customer."';");
    $conn->close();

    $data = $result->fetch_assoc();

    if($data == null)
        return null;
    else
        return $data;
}

function getPurchaseInfo($id_cart)
{
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $result = $conn->query("SELECT id_product,quantity FROM ps_cart_product WHERE id_cart='".$id_cart."';");
    $conn->close();

    $data = $result->fetch_assoc();

    if($data == null)
        return null;
    else
    {
        $quantity = $data['quantity'];
        $id_product = $data['id_product'];
        $profile = intval(getProductFeature($id_product, "Profilo"));
        $duration = getProductFeature($id_product, "Durata");
        $durationInt = intval(substr($duration, 0, -1));
        $durationType = substr($duration, -1);
        return array("profile"=>$profile, "duration"=>($durationInt*$quantity)."".$durationType);
    }
}

function getProductFeature($id_product, $featureName)
{
    return getFeatureValue($id_product, getIdFeature($featureName));
}


function getIdFeature($featureName)
{
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $result = $conn->query("SELECT id_feature FROM ps_feature_lang WHERE name='".$featureName."';");
    $conn->close();

    return $result->fetch_assoc()['id_feature'];
}

function getFeatureValue($id_product, $id_feature)
{
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $result = $conn->query("SELECT id_feature_value FROM ps_feature_product WHERE id_feature='".$id_feature."' AND id_product='".$id_product."';");

    $id_feature_value = $result->fetch_assoc()['id_feature_value'];
    $result2 = $conn->query("SELECT value FROM ps_feature_value_lang WHERE id_feature_value='".$id_feature_value."';");
    $conn->close();

    return $result2->fetch_assoc()['value'];
}

function generateUser($email)
{
    $user = explode("@", $email)[0];
    if(strlen($user) > 20)
        return substr($user, 0, 20);
    else
        return $user;
}

//EXTERNAL

function generateRandomString($length = 8) 
{
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

function add_months($months, DateTime $dateObject) 
{
    $next = new DateTime($dateObject->format('Y-m-d'));
    $next->modify('last day of +'.$months.' month');

    if($dateObject->format('d') > $next->format('d')) {
        return $dateObject->diff($next);
    } else {
        return new DateInterval('P'.$months.'M');
    }
}

function endCycle($d1, $months)
{
    $date = new DateTime($d1);

    // call second function to add the months
    $newDate = $date->add(add_months($months, $date));

    // goes back 1 day from date, remove if you want same day of month
    $newDate->sub(new DateInterval('P1D')); 

    //formats final date to Y-m-d form
    $dateReturned = $newDate->format('Y-m-d'); 

    return $dateReturned;
}

function parseJson($server)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, "http://".urlencode($server).":8080/index.php/rubrica");
    $result = curl_exec($ch);
    curl_close($ch);

    $obj = json_decode($result, true);

    return $obj;
}

function normalizeTimestamp($timestamp) //AAAA-MM-GG
{
    return substr($timestamp, 8)."/".substr($timestamp, 5, 2)."/".substr($timestamp, 0, 4);
}
?>