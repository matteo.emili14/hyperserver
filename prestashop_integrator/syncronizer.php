<?php
require_once('config.php');

updateAvailabilityOnPrestaShop();

function getTotalAvailability($serverList, $slotPerServer)
{
    $totalFreeSlot = 0; //slot totali liberi nel network

    for($i = 0; $i < count($serverList); $i++)
    {
        if($serverList[$i] != null) //controllo per mantenere allineati posizione elementi con nome (01 non è stato assegnato quindi manca tale posizione che è null)
        {
            $jsonObject = parseJson($serverList[$i]);
            
            if(count($jsonObject) < $slotPerServer) //se gli elementi in rubrica di un server sono minori degli slot massimi -> spazio disponibile
            {
                if($jsonObject['error']['code'] == "204")
                {
                    $serverFreeSlot = $slotPerServer;
                }
                else
                {
                    $serverFreeSlot = $slotPerServer - count($jsonObject);
                }
                $totalFreeSlot = $totalFreeSlot + $serverFreeSlot;
            }
        }
    }

    if($totalFreeSlot < 0)
        return 0;
    else
        return $totalFreeSlot;
}

function updateAvailabilityOnPrestaShop()
{
    global $serverList, $slotPerServer;

    $totalFreeSlot = getTotalAvailability($serverList, $slotPerServer);

    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $conn->query("UPDATE ps_stock_available SET quantity='".$totalFreeSlot."';");
    $conn->close();
}

function parseJson($server)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, "http://".$server.":8080/index.php/rubrica");
    $result = curl_exec($ch);
    curl_close($ch);

    $obj = json_decode($result, true);

    return $obj;
}

?>