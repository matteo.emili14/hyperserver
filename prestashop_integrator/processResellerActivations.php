<?php
require_once('config.php');

function checkForResellerOrdersToProcess()
{
    global $servername, $username, $password, $dbname2, $serverList, $slotPerServer;
    $conn = new mysqli($servername, $username, $password, $dbname2);
    
    $resellerOrders = getOrders();

    if($resellerOrders == null)
    {
        return;
    }
    else
    {
        while($row = $resellerOrders->fetch_assoc())
        {            
            echo "ok";
            $resellerEmail = $row['resellerEmail'];
            $clientEmail = $row['clientEmail'];
            $firstName = "RESELLER";
            $lastName = "RESELLER";
            $duration = 1; //RESELLER POSSONO VENDERE SOLO MENSILE (1 MESE)
            $profile = $row['profilo'];

            $serverData = hasAvailableSlot($serverList, $slotPerServer);
            $server = $serverData['server'];
            $port = $serverData['port'];
            //trovare server e porta disponibile

            $user = generateUser($clientEmail);

            manageActivation($firstName, $lastName, $user, array("clientEmail" => $clientEmail, "resellerEmail" => $resellerEmail), $profile, $server, $port, $duration);

            $conn->query("DELETE FROM `resellers_activation_queue` WHERE  `id`=".$row['id'].";"); //eliminare riga
            $conn->query("INSERT INTO `usersFromReseller` (`reseller`, `client`) VALUES ('".$resellerEmail."', '".$clientEmail."') ON DUPLICATE KEY UPDATE client='".$clientEmail."';"); //eliminare riga
        }
    }
    $conn->close();
}

function manageActivation($firstName, $lastName, $user, $emails, $profile, $server, $port, $duration)
{
    require_once(dirname(__FILE__).'/gmail.php');

    global $servername, $username, $password, $dbname2;
    $conn = new mysqli($servername, $username, $password, $dbname2);

    $result = $conn->query("SELECT * FROM rubrica_globale WHERE email='".$emails['clientEmail']."';");

    $data = $result->fetch_assoc();

    if($result->num_rows > 0) //controllo se ha/avuto abbonamento
    {
        if(time() < strtotime($data['dataTermine'])) //controllo se è attualmente in essere
        {
            //prolungo abbonamento o eseguo upgrade
            $activeServerPort = $data['porta'];
            $activeServer = $data['server'];
            $activeProfile = $data['profilo'];

            $endDate = updateCustomer($emails, $profile, $activeServer, $activeServerPort, $duration, true);

            if($profile != $activeProfile) //è anche un upgrade
            {
                configureServer($user, $profile, $activeServer, $activeServerPort, $data['password'], true); //eseguo upgrade
            }
        }
        else //abbonamento scaduto
        {
            //non c'è necessità di creare un nuovo utente, basta configurare un server con i dati già presenti
            $conn->query("UPDATE `rubrica_globale` SET `scaduto`='0' WHERE `email`='".$emails['clientEmail']."';");
            $endDate = updateCustomer($emails, $profile, $server, $port, $duration, false);
            configureServer($user, $profile, $server, $port, $data['password'], false);
        }
    }
    else //mai avuto abbonamento
    {
        $sshPassword = generateRandomString();
        $endDate = createCustomer($firstName, $lastName, $emails, $sshPassword, $profile, $server, $port, $duration);
        configureServer($user, $profile, $server, $port, $sshPassword, false);
    }
    $conn->close();
}

function findEhi($server, $port)
{
    $baseLocalPath = "/var/www/html/ehi/";
    $baseOnlinePath = "http://hyperspeed.it/ehi/";
    $id = substr($port, -1);
    $serverPath = explode(".", $server);
    $ext = ".ehi";

    $timLocalPath = $baseLocalPath.$serverPath[0]."/".$id."-tim".$ext;
    $vodafoneLocalPath = $baseLocalPath.$serverPath[0]."/".$id."-vodafone".$ext;

    $timOnlinePath = $baseOnlinePath.$serverPath[0]."/".$id."-tim".$ext;
    $vodafoneOnlinePath = $baseOnlinePath.$serverPath[0]."/".$id."-vodafone".$ext;

    return array("timLocalPath"=>$timLocalPath, "vodafoneLocalPath"=>$vodafoneLocalPath, "timOnlinePath"=>$timOnlinePath, "vodafoneOnlinePath"=>$vodafoneOnlinePath);
}

function createCustomer($firstName, $lastName, $emails, $sshPassword, $profile, $server, $port, $duration)
{
    global $servername, $username, $password, $dbname2;
    $conn = new mysqli($servername, $username, $password, $dbname2);

    $todayTimestamp = date('Y-m-d', time());
    $duration = endCycle($todayTimestamp, $duration);
    //0000-00-00 ANNO-MESE-GIORNO
    $conn->query("INSERT INTO rubrica_globale (`nome`, `cognome`, `email`, `password`, `profilo`, `server`, `porta`, `dataInizio`, `dataTermine`, `resellerEmail`) VALUES ('".$firstName."', '".$lastName."', '".$emails['clientEmail']."', '".$sshPassword."','".$profile."', '".$server."', '".$port."', '".date('Y-m-d H:i:s', time())."', '".$duration." 00:00:00', '".$emails['resellerEmail']."');");
    $conn->close();

    return $duration;
    //da eseguire solo se l'utente NON esiste già
}

function updateCustomer($emails, $profile, $server, $port, $duration, $alreadyActive)
{
    global $servername, $username, $password, $dbname2;
    $conn = new mysqli($servername, $username, $password, $dbname2);
    //0000-00-00 ANNO-MESE-GIORNO
    
    if($alreadyActive) //abbonamento attivo da prolungare
    {
        $result = $conn->query("SELECT date(dataTermine) AS dataTermine FROM rubrica_globale WHERE email='".$emails['clientEmail']."';");
        $timestampEndDate = $result->fetch_assoc()['dataTermine'];
        
        $duration = endCycle($timestampEndDate, $duration);

        $conn->query("UPDATE rubrica_globale SET profilo='".$profile."', dataTermine='".$duration." 00:00:00' WHERE email='".$emails['clientEmail']."';");
        $conn->close();

        return $duration;
    }
    else //abbonamento scaduto da rinnovare
    {
        
        $todayTimestamp = date('Y-m-d', time());
        $duration = endCycle($todayTimestamp, $duration);

        $conn->query("UPDATE rubrica_globale SET profilo='".$profile."', server='".$server."', porta='".$port."', dataInizio='".date('Y-m-d H:i:s', time())."', dataTermine='".$duration." 00:00:00' WHERE email='".$emails['clientEmail']."';");
        $conn->close();

        return $duration;
    }
}

function configureServer($user, $profile, $server, $port, $sshPassword, $alreadyActive = false)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if($alreadyActive)
    {
        //upgrade/downgrade di utenza già attiva
        curl_setopt($ch, CURLOPT_URL, "http://".urlencode($server).":8080/receiver.php?porta=".$port."&profilo=".$profile."&type=upgrade");
    }
    else
    {
        //hyperserver03.ns01.info:8080/receiver.php?porta=83&utente=matem&password=Pexe8eVb&profilo=30&type=aggiungi
        curl_setopt($ch, CURLOPT_URL, "http://".urlencode($server).":8080/receiver.php?porta=".$port."&utente=".urlencode($user)."&password=".urlencode($sshPassword)."&profilo=".$profile."&type=aggiungi");
        //configurazione ex novo
    }

    curl_exec($ch);
    curl_close($ch);
}

function hasAvailableSlot($serverList, $slotPerServer)
{
    $server = "";
    $port = 0;
    $available = false;
    for($i = 0; $i < count($serverList); $i++)
    {
        if($serverList[$i] != null) //controllo per mantenere allineati posizione elementi con nome (01 non è stato assegnato quindi manca tale posizione che è null)
        {
            $jsonObject = parseJson($serverList[$i]);
            if(count($jsonObject) < $slotPerServer) //se gli elementi in rubrica di un server sono minori degli slot massimi -> spazio disponibile
            {
                $server = $serverList[$i];
                $port = getAvailablePortOnServer($jsonObject);
                $available = true;
                break;
            }
        }
    }

    if($available)
        return array("server"=>$server, "port"=>$port);
    else
        return;
}

function getAvailablePortOnServer($jsonWithData)
{
    global $sshPorts;
    $alreadyUsedPorts = array();

    for($i = 0; $i < count($jsonWithData); $i++)
    {
        array_push($alreadyUsedPorts, intval($jsonWithData[$i]['porta']));
    }
    $freePorts = array_diff($sshPorts, $alreadyUsedPorts);

    return reset($freePorts); //prendo la prima porta libera;
}

function getOrders()
{
    global $servername, $username, $password, $dbname2;
    $conn = new mysqli($servername, $username, $password, $dbname2);
    $result = $conn->query("SELECT * FROM resellers_activation_queue;");
    $conn->close();

    return $result;
}

function getCustomerDetails($email) //NON è LO STESSO DI PROCESSACTIVATIONS.PHP
{
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    $result = $conn->query("SELECT firstname,lastname FROM ps_customer WHERE id_customer='".$email."';");
    $conn->close();

    $data = $result->fetch_assoc();

    if($data == null)
        return null;
    else
        return $data;
}

function generateUser($email)
{
    $user = explode("@", $email)[0];
    if(strlen($user) > 20)
        return substr($user, 0, 20);
    else
        return $user;
}

//EXTERNAL

function generateRandomString($length = 8) 
{
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

function add_months($months, DateTime $dateObject) 
{
    $next = new DateTime($dateObject->format('Y-m-d'));
    $next->modify('last day of +'.$months.' month');

    if($dateObject->format('d') > $next->format('d')) {
        return $dateObject->diff($next);
    } else {
        return new DateInterval('P'.$months.'M');
    }
}

function endCycle($d1, $months)
{
    $date = new DateTime($d1);

    // call second function to add the months
    $newDate = $date->add(add_months($months, $date));

    // goes back 1 day from date, remove if you want same day of month
    $newDate->sub(new DateInterval('P1D')); 

    //formats final date to Y-m-d form
    $dateReturned = $newDate->format('Y-m-d'); 

    return $dateReturned;
}

function parseJson($server)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, "http://".urlencode($server).":8080/index.php/rubrica");
    $result = curl_exec($ch);
    curl_close($ch);

    $obj = json_decode($result, true);

    return $obj;
}

function normalizeTimestamp($timestamp) //AAAA-MM-GG
{
    return substr($timestamp, 8)."/".substr($timestamp, 5, 2)."/".substr($timestamp, 0, 4);
}
?>