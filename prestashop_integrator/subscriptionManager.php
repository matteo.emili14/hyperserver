<?php
require_once('config.php');

checkSubscription();

function checkSubscription()
{
    global $servername, $username, $password, $dbname2;
    $conn = new mysqli($servername, $username, $password, $dbname2);

    $result = $conn->query("SELECT email,profilo,password,server,porta,scaduto FROM rubrica_globale WHERE UNIX_TIMESTAMP(date(dataTermine)) < UNIX_TIMESTAMP() AND scaduto = 0;");

    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, "http://".urlencode($row['server']).":8080/receiver.php?porta=".$row['porta']."&profilo=".$row['profilo']."&type=rimuovi");
            curl_exec($ch);
            curl_close($ch);
            $conn->query("UPDATE `rubrica_globale` SET `scaduto`='1' WHERE `email`='".$row['email']."' AND `porta`='".$row['porta']."';");
        }
    }
    $conn->close();
}
?>