<?php
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 * This uses traditional id & password authentication - look at the gmail_xoauth.phps
 * example to see how to use XOAUTH2.
 * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
 */

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;

require dirname(__FILE__).'/vendor/autoload.php';

//Create a new PHPMailer instance

function sendMail($email, $user, $firstName, $password, $endDate, $ehiPaths, $alreadyActive = false, $newClient = true)
{
    $mail = new PHPMailer;

    //Tell PHPMailer to use SMTP
    $mail->isSMTP();

    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;

    //Set the hostname of the mail server
    $mail->Host = 'smtp.gmail.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 587;

    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = "hyperserver01@gmail.com";

    //Password to use for SMTP authentication
    $mail->Password = "Pexe8eVb";

    //Set who the message is to be sent from
    $mail->setFrom('admin@hyperspeed.it', 'Hyperspeed.it');

    //Set an alternative reply-to address
    $mail->addReplyTo('admin@hyperspeed.it', 'Hyperspeed.it');

    //Set who the message is to be sent to
    $mail->addAddress($email, $firstName);

    //Set the subject line
    if($alreadyActive == false) //cliente con nessuna linea attiva
        $mail->Subject = 'La tua linea per la navigazione illimitata';
    else //cliente con linea attiva -> upgrade/estensione
        $mail->Subject = '=?UTF-8?B?'.base64_encode("Estensione di validità linea").'?=';

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    #$mail->msgHTML(file_get_contents('contents.html'), __DIR__);

    $signature = "<br><b>Suggerimento</b>: Puoi controllare lo stato della tua linea direttamente dal pannello di controllo del sito Hyperspeed.it<br><br>Grazie per aver utilizzato i servizi di Hyperspeed.it";

    //Replace the plain text body with one created manually
    if($alreadyActive == true)
        $mail->msgHTML('Il periodo di validità della tua linea è stato esteso al giorno '.$endDate.', eventuali upgrade o downgrade richiesti sono stati effettuati.'.$signature);
    else if($newClient == true)
    {
        $mail->msgHTML('La tua linea per navigare è stata generata con le seguenti credenziali:<br>
        <br>
        Utente: '.$user.'<br>
        Password: '.$password.'<br>
        Scadenza: '.$endDate.'<br>
        <br>
        Trovi in allegato i file, per ogni operatore supporato dal sistema, necessari per abilitare la navigazione illimitata e le istruzioni su come utilizzarli.'.$signature);
    }
    else if($newClient == false)
    {
        $mail->msgHTML('La tua linea per navigare è stata generata mantenendo le credenziali già in tuo possesso, di seguito un riepilogo per comodità:<br>
        <br>
        Utente: '.$user.'<br>
        Password: '.$password.'<br>
        Scadenza: '.$endDate.'<br>
        <br>
        Trovi in allegato i file, per ogni operatore supporato dal sistema, necessari per abilitare la navigazione illimitata e le istruzioni su come utilizzarli.<br>'.$signature);
    }

    //Attach an image file
    if($alreadyActive == false)
    {
        $mail->addAttachment($ehiPaths['timLocalPath']);
        $mail->addAttachment($ehiPaths['vodafoneLocalPath']);
    }
   

    //send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message sent!";
        //Section 2: IMAP
        //Uncomment these to save your message in the 'Sent Mail' folder.
        /*if (save_mail($mail)) {
            echo "Message saved!";
        }*/
    }
}

//Section 2: IMAP
//IMAP commands requires the PHP IMAP Extension, found at: https://php.net/manual/en/imap.setup.php
//Function to call which uses the PHP imap_*() functions to save messages: https://php.net/manual/en/book.imap.php
//You can use imap_getmailboxes($imapStream, '/imap/ssl') to get a list of available folders or labels, this can
//be useful if you are trying to get this working on a non-Gmail IMAP server.
function save_mail($mail)
{
    //You can change 'Sent Mail' to any other folder or tag
    $path = "{imap.gmail.com:993/imap/ssl}[Gmail]/Sent Mail";

    //Tell your server to open an IMAP connection using the same username and password as you used for SMTP
    $imapStream = imap_open($path, $mail->Username, $mail->Password);

    $result = imap_append($imapStream, $path, $mail->getSentMIMEMessage());
    imap_close($imapStream);

    return $result;
}