#!/bin/bash
apt update

apt -y install iptables-persistent
update-rc.d netfilter-persistent defaults

apt -y install dropbear
wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/configs/dropbear -O /etc/default/dropbear

apt -y install sudo
wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/configs/sudoers -O /etc/sudoers

apt -y install dsniff

apt -y install apache2 apache2-doc
wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/configs/ports.conf -O /etc/apache2/ports.conf

apt -y install mysql-server php5-mysql
mysql_secure_installation
wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/configs/my.cnf -O /etc/mysql/my.cnf

apt -y install php5-common libapache2-mod-php5 php5-cli

wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/configs/clienti.sql
#cat clienti.sql | mysql -u root -pPexe8eVb
mysql -u root -pPexe8eVb < clienti.sql
mysql -u root -pPexe8eVb -se "CREATE USER 'matteo'@'%' IDENTIFIED BY 'Pexe8eVb';"
mysql -u root -pPexe8eVb -se "GRANT ALL PRIVILEGES ON *.* TO 'matteo'@'%' WITH GRANT OPTION;"
mysql -u root -pPexe8eVb -se "FLUSH PRIVILEGES;"

wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/configs/html/config.php -P /var/www/html
wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/configs/html/receiver.php -P /var/www/html
wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/configs/html/index.php -P /var/www/html

useradd -s /bin/bash utente
echo utente:1234 | chpasswd

service mysql restart
service apache2 restart
service dropbear restart
service netfilter-persistent start

iptables -A INPUT -p tcp --syn --dport 80 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset
iptables -A INPUT -p tcp --syn --dport 81 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset
iptables -A INPUT -p tcp --syn --dport 82 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset
iptables -A INPUT -p tcp --syn --dport 83 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset
iptables -A INPUT -p tcp --syn --dport 84 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset
iptables -A INPUT -p tcp --syn --dport 85 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset
iptables -A INPUT -p tcp --syn --dport 86 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset
iptables -A INPUT -p tcp --syn --dport 87 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset
iptables -A INPUT -p tcp --syn --dport 88 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset
iptables -A INPUT -p tcp --syn --dport 89 -m connlimit --connlimit-above 1 -j REJECT --reject-with tcp-reset

tc qdisc add dev eth0 root handle 1: htb default 9

tc class add dev eth0 parent 1:0 classid 1:9 htb rate 1048576kbps ceil 1048576kbps prio 1
tc filter add dev eth0 parent 1:0 prio 1 protocol ip handle 9 fw flowid 1:9

#CLASSE LIMITATA
tc class add dev eth0 parent 1:0 classid 1:20 htb rate 1224kbps ceil 1224kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:21 htb rate 1224kbps ceil 1224kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:22 htb rate 1224kbps ceil 1224kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:23 htb rate 1224kbps ceil 1224kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:24 htb rate 1224kbps ceil 1224kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:25 htb rate 1224kbps ceil 1224kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:26 htb rate 1224kbps ceil 1224kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:27 htb rate 1224kbps ceil 1224kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:28 htb rate 1224kbps ceil 1224kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:29 htb rate 1224kbps ceil 1224kbps prio 0

tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 20 fw flowid 1:20
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 21 fw flowid 1:21
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 22 fw flowid 1:22
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 23 fw flowid 1:23
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 24 fw flowid 1:24
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 25 fw flowid 1:25
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 26 fw flowid 1:26
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 27 fw flowid 1:27
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 28 fw flowid 1:28
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 29 fw flowid 1:29

#CLASSE 30mb/s
tc class add dev eth0 parent 1:0 classid 1:300 htb rate 3900kbps ceil 3900kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:301 htb rate 3900kbps ceil 3900kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:302 htb rate 3900kbps ceil 3900kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:303 htb rate 3900kbps ceil 3900kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:304 htb rate 3900kbps ceil 3900kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:305 htb rate 3900kbps ceil 3900kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:306 htb rate 3900kbps ceil 3900kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:307 htb rate 3900kbps ceil 3900kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:308 htb rate 3900kbps ceil 3900kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:309 htb rate 3900kbps ceil 3900kbps prio 0

tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 300 fw flowid 1:300
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 301 fw flowid 1:301
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 302 fw flowid 1:302
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 303 fw flowid 1:303
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 304 fw flowid 1:304
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 305 fw flowid 1:305
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 306 fw flowid 1:306
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 307 fw flowid 1:307
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 308 fw flowid 1:308
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 309 fw flowid 1:309

#CLASSE 50mb/s
tc class add dev eth0 parent 1:0 classid 1:500 htb rate 6020kbps ceil 6020kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:501 htb rate 6020kbps ceil 6020kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:502 htb rate 6020kbps ceil 6020kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:503 htb rate 6020kbps ceil 6020kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:504 htb rate 6020kbps ceil 6020kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:505 htb rate 6020kbps ceil 6020kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:506 htb rate 6020kbps ceil 6020kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:507 htb rate 6020kbps ceil 6020kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:508 htb rate 6020kbps ceil 6020kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:509 htb rate 6020kbps ceil 6020kbps prio 0

tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 500 fw flowid 1:500
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 501 fw flowid 1:501
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 502 fw flowid 1:502
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 503 fw flowid 1:503
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 504 fw flowid 1:504
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 505 fw flowid 1:505
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 506 fw flowid 1:506
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 507 fw flowid 1:507
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 508 fw flowid 1:508
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 509 fw flowid 1:509

#CLASSE 100mb/s
tc class add dev eth0 parent 1:0 classid 1:1000 htb rate 11340kbps ceil 11340kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:1001 htb rate 11340kbps ceil 11340kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:1002 htb rate 11340kbps ceil 11340kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:1003 htb rate 11340kbps ceil 11340kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:1004 htb rate 11340kbps ceil 11340kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:1005 htb rate 11340kbps ceil 11340kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:1006 htb rate 11340kbps ceil 11340kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:1007 htb rate 11340kbps ceil 11340kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:1008 htb rate 11340kbps ceil 11340kbps prio 0
tc class add dev eth0 parent 1:0 classid 1:1009 htb rate 11340kbps ceil 11340kbps prio 0

tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1000 fw flowid 1:1000
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1001 fw flowid 1:1001
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1002 fw flowid 1:1002
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1003 fw flowid 1:1003
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1004 fw flowid 1:1004
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1005 fw flowid 1:1005
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1006 fw flowid 1:1006
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1007 fw flowid 1:1007
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1008 fw flowid 1:1008
tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1009 fw flowid 1:1009

invoke-rc.d netfilter-persistent save

wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/fairUsageController.sh
chmod +x fairUsageController.sh
crontab -l | { cat; echo "0 * * * * /bin/bash /root/fairUsageController.sh >/dev/null 2>&1"; } | crontab -


wget https://gitlab.com/matteo.emili14/hyperserver/raw/master/restoreTCRules.sh
chmod +x restoreTCRules.sh
crontab -l | { cat; echo "@reboot /bin/bash /root/restoreTCRules.sh"; } | crontab -


echo "INSTALLAZIONE COMPLETATA CON SUCCESSO"