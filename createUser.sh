#!/bin/sh
if [ -z "$1" ] || [ -z "$2" ] 
then
    echo "Parametri non validi, formato createUser.sh USER PASSWORD"
else
    useradd -s /bin/bash $1
    echo $1:$2 | chpasswd
fi
