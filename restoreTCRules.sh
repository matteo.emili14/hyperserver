#!/bin/bash
sleep 30

sudo tc qdisc add dev eth0 root handle 1: htb default 9

sudo tc class add dev eth0 parent 1:0 classid 1:9 htb rate 1048576kbps ceil 1048576kbps prio 1
sudo tc filter add dev eth0 parent 1:0 prio 1 protocol ip handle 9 fw flowid 1:9

#CLASSE LIMITATA
sudo tc class add dev eth0 parent 1:0 classid 1:20 htb rate 1224kbps ceil 1224kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:21 htb rate 1224kbps ceil 1224kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:22 htb rate 1224kbps ceil 1224kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:23 htb rate 1224kbps ceil 1224kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:24 htb rate 1224kbps ceil 1224kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:25 htb rate 1224kbps ceil 1224kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:26 htb rate 1224kbps ceil 1224kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:27 htb rate 1224kbps ceil 1224kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:28 htb rate 1224kbps ceil 1224kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:29 htb rate 1224kbps ceil 1224kbps prio 0

sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 20 fw flowid 1:20
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 21 fw flowid 1:21
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 22 fw flowid 1:22
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 23 fw flowid 1:23
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 24 fw flowid 1:24
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 25 fw flowid 1:25
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 26 fw flowid 1:26
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 27 fw flowid 1:27
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 28 fw flowid 1:28
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 29 fw flowid 1:29

#CLASSE 30mb/s
sudo tc class add dev eth0 parent 1:0 classid 1:300 htb rate 3900kbps ceil 3900kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:301 htb rate 3900kbps ceil 3900kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:302 htb rate 3900kbps ceil 3900kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:303 htb rate 3900kbps ceil 3900kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:304 htb rate 3900kbps ceil 3900kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:305 htb rate 3900kbps ceil 3900kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:306 htb rate 3900kbps ceil 3900kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:307 htb rate 3900kbps ceil 3900kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:308 htb rate 3900kbps ceil 3900kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:309 htb rate 3900kbps ceil 3900kbps prio 0

sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 300 fw flowid 1:300
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 301 fw flowid 1:301
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 302 fw flowid 1:302
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 303 fw flowid 1:303
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 304 fw flowid 1:304
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 305 fw flowid 1:305
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 306 fw flowid 1:306
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 307 fw flowid 1:307
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 308 fw flowid 1:308
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 309 fw flowid 1:309

#CLASSE 50mb/s
sudo tc class add dev eth0 parent 1:0 classid 1:500 htb rate 6020kbps ceil 6020kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:501 htb rate 6020kbps ceil 6020kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:502 htb rate 6020kbps ceil 6020kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:503 htb rate 6020kbps ceil 6020kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:504 htb rate 6020kbps ceil 6020kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:505 htb rate 6020kbps ceil 6020kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:506 htb rate 6020kbps ceil 6020kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:507 htb rate 6020kbps ceil 6020kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:508 htb rate 6020kbps ceil 6020kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:509 htb rate 6020kbps ceil 6020kbps prio 0

sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 500 fw flowid 1:500
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 501 fw flowid 1:501
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 502 fw flowid 1:502
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 503 fw flowid 1:503
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 504 fw flowid 1:504
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 505 fw flowid 1:505
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 506 fw flowid 1:506
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 507 fw flowid 1:507
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 508 fw flowid 1:508
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 509 fw flowid 1:509

#CLASSE 100mb/s
sudo tc class add dev eth0 parent 1:0 classid 1:1000 htb rate 11340kbps ceil 11340kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:1001 htb rate 11340kbps ceil 11340kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:1002 htb rate 11340kbps ceil 11340kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:1003 htb rate 11340kbps ceil 11340kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:1004 htb rate 11340kbps ceil 11340kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:1005 htb rate 11340kbps ceil 11340kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:1006 htb rate 11340kbps ceil 11340kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:1007 htb rate 11340kbps ceil 11340kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:1008 htb rate 11340kbps ceil 11340kbps prio 0
sudo tc class add dev eth0 parent 1:0 classid 1:1009 htb rate 11340kbps ceil 11340kbps prio 0

sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1000 fw flowid 1:1000
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1001 fw flowid 1:1001
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1002 fw flowid 1:1002
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1003 fw flowid 1:1003
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1004 fw flowid 1:1004
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1005 fw flowid 1:1005
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1006 fw flowid 1:1006
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1007 fw flowid 1:1007
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1008 fw flowid 1:1008
sudo tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 1009 fw flowid 1:1009

