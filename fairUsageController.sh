#!/bin/bash
#CONFIG
dbPassword="Pexe8eVb"
dbUser="root"
dbName="clienti"

converter()
{
    if [ "${1: -1}" == "M" ]; then
        result=${1::-1}
    elif [ "${1: -1}" == "K" ]; then
        result=1
    elif [ "${1: -1}" == "G" ]; then
        result=$((${1::-1} * 1000))
    else
        result=1
    fi  
}

limitaUtente() #1 = porta, $2 = profilo
{
    sudo mysql $dbName -u $dbUser -p$dbPassword -se "UPDATE rubrica SET limitato=1 WHERE porta='$1';"
    sudo iptables -D OUTPUT -t mangle -p tcp --sport $1 -j MARK --set-mark $2${1: -1} #rimuovo regola standard
    sudo iptables -A OUTPUT -t mangle -p tcp --sport $1 -j MARK --set-mark 2${1: -1} #applico regola limitata
}

rimuoviLimitazione() #1 = porta, $2 = profilo
{
    sudo mysql $dbName -u $dbUser -p$dbPassword -se "UPDATE rubrica SET limitato=0 WHERE porta='$1';"
    sudo iptables -D OUTPUT -t mangle -p tcp --sport $1 -j MARK --set-mark 2${1: -1} #rimuovo regola limitata
    sudo iptables -A OUTPUT -t mangle -p tcp --sport $1 -j MARK --set-mark $2${1: -1} #applico regola standard
}

controllaTraffico() #1 = porta, 2= profilo, 3 = dati orari, 4 = stato utente (limitato)
{
    if [ $2 == "30" ]; then #profilo 30 mega
        if [ $3 -gt "10000" ]; then #limite 10 gb orari
            if [ $4 == "0" ]; then #stato limitazione utente
                #echo "Profilo 30 mega, traffico superiore a 10gb, utente non limitato -> lo limito"
                limitaUtente $1 "30"
                #applico limitazione
            fi
        else #se non ha superato il limite
            if [ $4 == "1" ]; then #controllo stato limitazione, se attivo rimuovo NON VA QUI
                #echo "Profilo 30 mega, traffico inferiore a 10gb, utente limitato -> lo ripristino"
                rimuoviLimitazione $1 "30"
                #rimuovo limitazione
            fi
        fi
    elif [ $2 == "50" ]; then #profilo 50 mega
        if [ $3 -gt "40000" ] ; then #limite 40gb orari
            if [ $4 == "0" ]; then #stato limitazione utente
                #echo "Profilo 50 mega, traffico superiore a 40gb, utente non limitato -> lo limito"
                limitaUtente $1 "50"
                #applico limitazione
            fi
        else #se non ha superato il limite
            if [ $4 == "1" ]; then #controllo stato limitazione, se attivo rimuovo
                #echo "Profilo 50 mega, traffico inferiore a 40gb, utente limitato -> lo ripristino"
                rimuoviLimitazione $1 "50"
                #rimuovo limitazione
            fi
        fi
    elif [ $2 == "100" ]; then #profilo 100 mega
        if [ $3 -gt "80000" ]; then #limite 80gb orari
            if [ $4 == "0" ]; then #stato limitazione utente
                #echo "Profilo 100 mega, traffico superiore a 80gb, utente non limitato -> lo limito"
                limitaUtente $1 "100"
                #applico limitazione
            fi
        else #se non ha superato il limite
            if [ $4 == "1" ]; then #controllo stato limitazione, se attivo rimuovo
                #echo "Profilo 100 mega, traffico inferiore a 80gb, utente limitato -> lo ripristino"
                rimuoviLimitazione $1 "100"
                #rimuovo limitazione
            fi
        fi
    fi
}

port80=$(sudo iptables -vL -t mangle | sed -n '/spt:http/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1' | awk '{gsub(/http+/,80)}1')
port81=$(sudo iptables -vL -t mangle | sed -n '/spt:81/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1')
port82=$(sudo iptables -vL -t mangle | sed -n '/spt:82/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1')
port83=$(sudo iptables -vL -t mangle | sed -n '/spt:83/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1')
port84=$(sudo iptables -vL -t mangle | sed -n '/spt:84/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1')
port85=$(sudo iptables -vL -t mangle | sed -n '/spt:85/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1')
port86=$(sudo iptables -vL -t mangle | sed -n '/spt:86/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1')
port87=$(sudo iptables -vL -t mangle | sed -n '/spt:link/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1' | awk '{gsub(/link+/,87)}1')
port88=$(sudo iptables -vL -t mangle | sed -n '/spt:kerberos/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1' | awk '{gsub(/kerberos+/,88)}1')
port89=$(sudo iptables -vL -t mangle | sed -n '/spt:89/p' | awk 'BEGIN {ORS=" "};{print $2}{print $11}' | awk -F'spt:' 'BEGIN {ORS=" "};{print $1}{print $2}{printf "\n"}' | awk '{gsub(/[ ]+/," ")}1' | awk '{gsub(/kerberos+/,89)}1')

arrayData[0]=$port80
arrayData[1]=$port81
arrayData[2]=$port82
arrayData[3]=$port83
arrayData[4]=$port84
arrayData[5]=$port85
arrayData[6]=$port86
arrayData[7]=$port87
arrayData[8]=$port88
arrayData[9]=$port89

i=0
until [ $i -gt ${#arrayData[@]} ]
do
	array=(${arrayData[$i]})
    datiPort=$(sudo mysql $dbName -u $dbUser -p$dbPassword -se "SELECT dati FROM rubrica WHERE porta='${array[1]}';")
    if( [[ !  -z  $datiPort  ]])
    then
        converter ${array[0]}
        datiOrariPort=$(($result - $datiPort))
        #if [ $datiOrariPort -gt 0 ]; then
            sudo mysql $dbName -u $dbUser -p$dbPassword -se "UPDATE rubrica SET dati=$result WHERE porta='${array[1]}';"
            sudo mysql $dbName -u $dbUser -p$dbPassword -se "UPDATE rubrica SET datiOrari=$datiOrariPort WHERE porta='${array[1]}';"
            sudo mysql $dbName -u $dbUser -p$dbPassword -se "UPDATE logDati SET datiTotaliMB=datiTotaliMB+$datiOrariPort WHERE porta='${array[1]}';" #ogni ora aggiorno computo totale dati scaricati
        #else
        #    sudo mysql $dbName -u $dbUser -p$dbPassword -se "UPDATE rubrica SET dati=$result WHERE porta='${array[1]}';"
        #    sudo mysql $dbName -u $dbUser -p$dbPassword -se "UPDATE rubrica SET datiOrari=$result WHERE porta='${array[1]}';" #primo run dopo limitazione -> dati orari uguali a dati totali
        #    sudo mysql $dbName -u $dbUser -p$dbPassword -se "UPDATE logDati SET datiTotaliMB=datiTotaliMB+$result WHERE porta='${array[1]}';" #ogni ora aggiorno computo totale dati scaricati
        #fi

        profilo=$(sudo mysql $dbName -u $dbUser -p$dbPassword -se "SELECT profilo FROM rubrica WHERE porta='${array[1]}';")
        limitato=$(sudo mysql $dbName -u $dbUser -p$dbPassword -se "SELECT limitato FROM rubrica WHERE porta='${array[1]}';")
        controllaTraffico ${array[1]} $profilo $datiOrariPort $limitato #1 = porta, 2= profilo, 3 = dati orari, 4 = stato utente (limitato)
    fi
    i=$(( i+1 ))
done

sudo invoke-rc.d netfilter-persistent save